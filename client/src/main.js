// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueI18n from "vue-i18n"
import App from './App'
import store from './stores'
import axios from "axios"
//import 'milligram-sass'
import './styles/app.scss'
import config from '../../server/config/config'
import router from '@/router/index'
import fr from '@/translations/fr'
import './registerServiceWorker'
import Default from '@/layouts/Default'
import Admin from '@/layouts/Admin'
import Dashboard from '@/layouts/Dashboard'

Vue.component("default-layout", Default);
Vue.component("admin-layout", Admin);
Vue.component("dashboard-layout", Dashboard);

Vue.config.productionTip = false
Vue.use(VueI18n);

Vue.prototype.$http = global.$http = axios.create({
  // base url model: https://mydomain:port/url/version
  baseURL: `https://${config.api.host}:${config.api.port}/${config.api.url}/v${config.api.version}`,
});
const token = localStorage.getItem('token');
if (token) {
  Vue.prototype.$http.defaults.headers.common['Authorization'] = token
}

const messages = {
  fr: fr
}

const i18n = new VueI18n({
  locale: 'fr', // set locale
  messages, // set locale messages
});

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount("#app");