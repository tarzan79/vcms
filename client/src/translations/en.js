/**
 * english traduction
 */
export default {
  message: {
    index: 'Home',
    login: 'Login',
    logout: 'Logout'
  }
}