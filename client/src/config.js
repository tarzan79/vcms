import config from "../../server/config/config";

export const server = {
  "protocol": "http",
  "host": config.server.host,
  "port": config.server.port,
  "version": config.server.version
};
export const api = {
  "url": `http://${server.host}:${server.port}/api/v${server.version}`
}
